import unittest
from parameterized import parameterized

from unittest.mock import mock_open, patch

from tests.unit.ccchanel.base_setup_ccchanelfile import TestCaseWith_CCChanelFile


class TestCCChanel_CCChanelFile_Write(TestCaseWith_CCChanelFile):
    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_write_open_args(self, mocked_open, mocked_isdir):
        path = "/tmp/client"
        self.chanel.write(b'data', path)
        mocked_open.assert_called_once_with(path, 'wb')

    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_write_write_args(self, mocked_open, mocked_isdir):
        data = b'data'
        self.chanel.write(data, "/tmp/client")
        mocked_open().write.assert_called_once_with(data)

    @parameterized.expand([
        ([], True),
        (["/tmp"], False),
        (["/tmp", "garbage"], True)
    ])
    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_write_args_number(self, args, expected_exception, mocked_open, mocked_isdir):
        if expected_exception:
            self.assertRaises(ValueError, self.chanel.write, b'data', *args)
        else:
            self.chanel.write(b'data', *args)

    @parameterized.expand([
        (True,),
        (False,),
    ])
    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_write_is_dir_exception(self, is_dir, mocked_open, mocked_isdir):
        mocked_isdir.configure_mock(return_value=is_dir)
        data = b'data'
        path = '/tmp'
        if is_dir:
            self.assertRaises(IsADirectoryError, self.chanel.write, data, path)
        else:
            self.chanel.write(data, path)


class TestCCChanel_CChanelFile_Read(TestCaseWith_CCChanelFile):

    @patch("os.path.isdir", return_value=False)
    def test_read_data(self, mocked_isdir):
        path = "/tmp"
        data = b'data'
        with patch('builtins.open', new_callable=mock_open, read_data=data) as mocked_open:
            read_data_chanel = self.chanel.read(path)
            self.assertEqual(data, read_data_chanel)

    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_read_open_arguments(self, mocked_open, mocked_isdir):
        path = "/tmp"
        self.chanel.read(path)
        mocked_open.assert_called_once_with(path, 'rb')

    @patch("os.path.isdir", return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_read_read_arguments(self, mocked_open, mocked_isdir):
        path = "/tmp"
        self.chanel.read(path)
        mocked_open().read.assert_called_once_with()

    @parameterized.expand([
        (True,),
        (False,),
    ])
    @patch("os.path.isdir")
    @patch('builtins.open', new_callable=mock_open)
    def test_read_is_dir_exception(self, is_dir, mocked_open, mocked_isdir):
        mocked_isdir.configure_mock(return_value=is_dir)
        path = '/tmp'
        if is_dir:
            self.assertRaises(IsADirectoryError, self.chanel.read, path)
        else:
            self.chanel.read(path)


class TestCCChanel_CChanelFile_List(TestCaseWith_CCChanelFile):

    @parameterized.expand([
        ('/tmp', ['file1', 'file2'])
    ])
    @patch("os.listdir")
    def test_list_data(self, path, wanted_listed_files, mock_listdir):
        mock_listdir.configure_mock(return_value=wanted_listed_files)
        chanel_list = self.chanel.list(path)
        self.assertEqual(wanted_listed_files, chanel_list)

    @parameterized.expand([
        ('/tmp', ['file1', 'file2'])
    ])
    @patch("os.listdir")
    def test_list_args(self, path, wanted_listed_files, mock_listdir):
        mock_listdir.configure_mock(return_value=wanted_listed_files)
        self.chanel.list(path)
        mock_listdir.assert_called_once_with(path)

    @parameterized.expand([
        (True,),
        (False,),
    ])
    @patch("os.path.isdir")
    @patch('builtins.open', new_callable=mock_open)
    def test_list_not_dir_exception(self, is_dir, mocked_open, mocked_isdir):
        mocked_isdir.configure_mock(return_value=is_dir)
        path = '/tmp'
        if not is_dir:
            self.assertRaises(NotADirectoryError, self.chanel.list, path)
        else:
            self.chanel.list(path)


class TestCCChanel_CChanelFile_Exists(TestCaseWith_CCChanelFile):

    @parameterized.expand([
        ('/tmp/file1', True)
    ])
    @patch("os.path.exists")
    def test_file_exists(self, the_file, expected_file_exists, mock_exists):
        mock_exists.configure_mock(return_value=expected_file_exists)
        file_exists = self.chanel.exists(the_file)
        self.assertEqual(file_exists, expected_file_exists)
