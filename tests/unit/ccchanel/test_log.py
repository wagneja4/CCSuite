from parameterized import parameterized
from unittest.mock import mock_open, patch
from ccsuite.ccchanel.ccchanel_log import log_init, log_append, log_read

from tests.unit.ccchanel.base_setup_ccchanelfile_ccstenobase import TestCaseWith_CCChanelFile_CCStenoBase
from tests.utils.parsers import *
from tests.utils.resources import load_test_data


class TestCCChanel_Log_Init(TestCaseWith_CCChanelFile_CCStenoBase):
    @parameterized.expand([
        ("client_id", b'[]')
    ])
    @patch("os.path.exists", return_value=False)
    @patch('os.path.isdir', return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_log_init(self, client_path, should_be_writen_with, mocked_open, mocked_isdir, mocked_exists):
        log_init(self.chanel, self.steno, client_path)
        mocked_open().write.assert_called_once_with(should_be_writen_with)

    @parameterized.expand([
        (True,),
        (False,)
    ])
    @patch("os.path.exists", return_value=False)
    @patch('os.path.isdir')
    @patch('builtins.open', new_callable=mock_open)
    def test_log_init_path_is_directory(self, path_is_directory, mocked_open, mocked_isdir, mocked_exists):
        mocked_isdir.configure_mock(return_value=path_is_directory)
        client_path = "/path"
        if not path_is_directory:
            log_init(self.chanel, self.steno, client_path)
        else:
            self.assertRaises(IsADirectoryError, log_init, self.chanel, self.steno, client_path)

    @parameterized.expand([
        (True,),
        (False,)
    ])
    @patch("os.path.exists")
    @patch('os.path.isdir', return_value=False)
    @patch('builtins.open', new_callable=mock_open)
    def test_log_init_path_exists(self, path_is_directory, mocked_open, mocked_isdir, mocked_exists):
        mocked_exists.configure_mock(return_value=path_is_directory)
        client_path = "/path"
        if not path_is_directory:
            log_init(self.chanel, self.steno, client_path)
        else:
            self.assertRaises(FileExistsError, log_init, self.chanel, self.steno, client_path)


class TestCCChanel_Log_Read(TestCaseWith_CCChanelFile_CCStenoBase):

    @parameterized.expand([
        ([], b'[]',),
        (["data"], b'["data"]'),
        ([{"test": "dictionary"}], b'[{"test": "dictionary"}]'),
    ])
    @patch('os.path.isdir', return_value=False)
    @patch("os.path.exists", return_value=True)
    def test_log_read_with_data_present(self, expected_read_data, mocked_read_data, mocked_exists, mocked_isdir):
        client_path = "/tmp/client_id"
        with patch('builtins.open', new_callable=mock_open, read_data=mocked_read_data) as mocked_open:
            data_read = log_read(self.chanel, self.steno, client_path)
            self.assertEqual(expected_read_data, data_read)

    @parameterized.expand([
        (b'', True),
        (b'{}', True),
        (b'test', True),
        (b'[]', False),
        (b'["test"]', False),
    ])
    @patch('os.path.isdir', return_value=False)
    @patch("os.path.exists", return_value=True)
    def test_log_read_corrupted_log(self, data_already_present, exception_expected, mock_exists, mocked_isdir):
        client_path = "/tmp/client_id"
        with patch('builtins.open', new_callable=mock_open, read_data=data_already_present) as mocked_open:
            if not exception_expected:
                log_read(self.chanel, self.steno, client_path)
            else:
                self.assertRaises(ValueError, log_read, self.chanel, self.steno, client_path)

    @parameterized.expand([
        (False,),
        (True,)
    ])
    @patch("os.path.isdir", return_value=False)
    @patch("os.path.exists")
    def test_log_read_with_nonexistent_file(self, file_present, mock_exists, mocked_isdir):
        client_path = "/tmp/client_id"
        data_already_present = b'[]'
        mock_exists.configure_mock(return_value=file_present)
        with patch('builtins.open', new_callable=mock_open, read_data=data_already_present) as mocked_open:
            if file_present:
                log_read(self.chanel, self.steno, client_path)
            else:
                self.assertRaises(FileNotFoundError, log_read, self.chanel, self.steno, client_path)

    @parameterized.expand([
        (True, ),
        (False, )
    ])
    @patch("os.path.isdir")
    @patch("os.path.exists", return_value=True)
    def test_log_read_with_path_is_directory(self, is_directory, mock_exists, mocked_isdir):
        client_path = "/tmp/client_id"
        object_to_write = "object"
        data_already_present = b'[]'
        mocked_isdir.configure_mock(return_value=is_directory)
        with patch('builtins.open', new_callable=mock_open, read_data=data_already_present) as mocked_open:
            if not is_directory:
                log_read(self.chanel, self.steno, object_to_write, client_path)
            else:
                self.assertRaises(IsADirectoryError, log_read, self.chanel, self.steno, client_path)


class TestCCChanel_Log_Append(TestCaseWith_CCChanelFile_CCStenoBase):
    @parameterized.expand(
        load_test_data('ccchanel/test_log/append/correct_append.csv',
                       parse_bytes,
                       parse_json_or_string,
                       parse_bytes,
                       parse_bool))
    @patch('os.path.isdir', return_value=False)
    @patch("os.path.exists", return_value=True)
    def test_log_append_correct_append(self, data_already_present, object_to_write, expected_writen_data, is_correct,
                                       mock_exists, mocked_isdir):
        client_path = "/tmp/client_id"
        with patch('builtins.open', new_callable=mock_open, read_data=data_already_present) as mocked_open:
            log_append(self.chanel, self.steno, object_to_write, client_path)
            if is_correct:
                mocked_open().write.assert_called_once_with(expected_writen_data)
            else:
                self.assertRaises(AssertionError, mocked_open().write.assert_called_once_with, expected_writen_data)
