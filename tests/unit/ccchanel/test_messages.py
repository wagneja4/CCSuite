import unittest
from parameterized import parameterized
from unittest.mock import mock_open, patch

from ccsuite.ccchanel.ccchanel_messages import *


class TestCCChanel_Messeges(unittest.TestCase):
    @parameterized.expand([
        ('exec', ['test', ], 12345, {'command': {'args': ['test'], 'id': 12345, 'kind': 'exec', 'timestamp': 12345}}),
    ])
    @patch('time.time')
    def test_server_create_command(self,
                                   command_kind,
                                   command_args,
                                   wanted_timestamp,
                                   obj_wanted,
                                   mocked_time):
        mocked_time.configure_mock(return_value=wanted_timestamp)
        created_command_object = message_create_command(wanted_timestamp, command_kind, *command_args)
        self.assertDictEqual(obj_wanted, created_command_object)

    @parameterized.expand([
        (12345, {'ping': {'timestamp': 12345}}),
    ])
    @patch('time.time')
    def test_server_create_ping(self,
                                timestamp_mocked,
                                obj_wanted,
                                mocked_time):
        mocked_time.configure_mock(return_value=timestamp_mocked)
        created_command_object = message_create_ping()
        self.assertDictEqual(obj_wanted, created_command_object)

    @parameterized.expand([
        (12345, {'alive': {'timestamp': 12345}}),
    ])
    @patch('time.time')
    def test_server_create_alive(self,
                                 timestamp_mocked,
                                 obj_wanted,
                                 mocked_time):
        mocked_time.configure_mock(return_value=timestamp_mocked)
        created_command_object = message_create_alive()
        self.assertDictEqual(obj_wanted, created_command_object)

    @parameterized.expand([
        (12345, {}, {'done': {'id': 12345, 'payload': {}}}),
    ])
    def test_server_create_done(self,
                                job_id,
                                payload,
                                obj_wanted):
        created_command_object = message_create_done(job_id, payload)
        self.assertDictEqual(obj_wanted, created_command_object)
