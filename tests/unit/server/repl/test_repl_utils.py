import unittest
import os

from parameterized import parameterized
from lark.exceptions import UnexpectedCharacters, UnexpectedEOF

from tests.utils.resources import load_test_data
from ccsuite.server.repl.parser.parser_default import ParserDefault
from tests.utils.parsers import *


class TestCCServer_ReplUtils_ParseCommand(unittest.TestCase):

    @parameterized.expand(
        load_test_data('server/repl_utils/ccsuite_repl-3_way.csv',
                       str,
                       lambda x: parse_string_with_replace(x, {'empty_string': ''}),
                       lambda x: parse_string_with_replace(x, {'empty_string': ''}),
                       int,
                       parse_bool,
                       debug=False
                       ))
    def test_repl_command(self, *args):
        invalid_combination = args[-1]
        number_of_args = args[-2]
        dummy_args = ['arg' for i in range(number_of_args)]
        actual_args = list(args[:-2])
        string_to_parse = ' '.join(actual_args + dummy_args) + "\n"
        language_spec_path = os.path.join(os.environ['CCSUITE_RESOURCES_ROOT'], 'server', 'repl', 'parser', 'repl.ebnf')
        parser = ParserDefault(path=language_spec_path)
        if invalid_combination:
            try:
                parser.parse(string_to_parse)
            except UnexpectedCharacters:
                pass
            except UnexpectedEOF:
                pass
        else:
            parser.parse(string_to_parse)
