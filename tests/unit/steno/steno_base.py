from parameterized import parameterized
import json
from tests.unit.steno.base_setup_ccstenobase import TestCaseWith_CCStenoBase
from tests.utils.parsers import parse_bytes
from tests.utils.resources import load_test_data


class TestCCStenoEncode(TestCaseWith_CCStenoBase):
    @parameterized.expand(
        load_test_data('steno/base/correct_encode_decode.csv',
                       json.loads,
                       parse_bytes))
    def test_encode_correctness(self, object_to_encode, wanted_bytes):
        encoded_object = self.steno.encode(object_to_encode)
        self.assertEqual(wanted_bytes, encoded_object)


class TestCCStenoDecode(TestCaseWith_CCStenoBase):
    @parameterized.expand(
        load_test_data('steno/base/correct_encode_decode.csv',
                       parse_bytes,
                       json.loads))
    def test_decode_correctness(self, object_to_decode, decoded_object):
        decoded_bytes = self.steno.decode(object_to_decode)
        self.assertEqual(decoded_object, decoded_bytes)
