from unittest.mock import patch

from tests.unit.client.base_setup_ccclienthostname import TestCaseWith_CCClientIdHostname


class TestCCClient_IDHostname(TestCaseWith_CCClientIdHostname):
    @patch('subprocess.run')
    def test_hostname_args(self, mocked_subprocess_run):
        self.id_provider.id()
        mocked_subprocess_run.assert_called_once_with(["hostname"], capture_output=True)

    # todo: broken mock :thinking:
    # @patch('subprocess.run')
    # def test_hostname_return_type(self, mocked_subprocess_run):
    #     actual_return = self.id_provider.id()
    #     self.assertIsInstance(actual_return, str)
