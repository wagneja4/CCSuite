import unittest

from ccsuite.client.id_hostname import CCClientIDHostname
from ccsuite.client.exec_subprocess import CCClientExecSubprocess


class TestCaseWith_CCClientIdHostname(unittest.TestCase):
    def setUp(self):
        self.id_provider = CCClientIDHostname(CCClientExecSubprocess())
