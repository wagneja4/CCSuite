from parameterized import parameterized
from unittest.mock import patch

from tests.unit.client.base_setup_ccclientexec import TestCaseWith_CCClientExec


class TestCCClient_ExecSubprocess(TestCaseWith_CCClientExec):
    @patch('subprocess.run')
    def test_exec_no_args(self, mocked_subprocess_run):
        self.assertRaises(ValueError, self.executor.exec)

    @parameterized.expand([
        (42, [], True),
        ("42", [1], True),
        ("42", ["24"], False),
    ])
    @patch('subprocess.run')
    def test_exec_arg_not_string(self, exec_arg, args, exception_expected, mocked_subprocess_run):
        if exception_expected:
            self.assertRaises(TypeError, self.executor.exec, exec_arg, *args)
        else:
            self.executor.exec(exec_arg, *args)

    @parameterized.expand([
        ("", [], True),
    ])
    @patch('subprocess.run')
    def test_exec_arg_empty_string(self, exec_arg, args, exception_expected, mocked_subprocess_run):
        if exception_expected:
            self.assertRaises(ValueError, self.executor.exec, exec_arg, *args)
        else:
            self.executor.exec(exec_arg, *args)

    # todo: broken mock :thinking:
    # @parameterized.expand([
    #     ("whoami", [], b'return bytes')
    # ])
    # @patch('subprocess.run')
    # def test_exec_correct_return(self, exec_arg, args, expected_return, mocked_subprocess_run):
    #     mocked_subprocess_run.configure_mock(stdout=expected_return)
    #     actual_return = self.executor.exec(exec_arg, *args)
    #     self.assertEqual(expected_return, actual_return)

    @parameterized.expand([
        ('whoami', [], ['whoami']),
        ('ls', ['/tmp'], ['ls', '/tmp']),
    ])
    @patch('subprocess.run')
    def test_exec_correct_return(self, exec_arg, args, expected_args, mocked_subprocess_run):
        self.executor.exec(exec_arg, *args)
        mocked_subprocess_run.assert_called_once_with(expected_args, capture_output=True)
