import csv
import json


def parse_csv(csv_path, collumn_parser_list, delimiter=';', quotechar='"', comment_char='#',debug=False):
    csv_array = []
    with open(csv_path, mode='r') as file:
        csv_file_iterator = csv.reader(file, delimiter=delimiter, quotechar=quotechar)
        for line in csv_file_iterator:
            csv_line = []
            if debug:
                print(line)
            if line[0][0] == comment_char:
                if debug:
                    print("skipping", line)
                continue
            for i in range(len(collumn_parser_list)):
                if debug:
                    print(collumn_parser_list[i], line[i])
                csv_line.append(collumn_parser_list[i](line[i]))
            csv_array.append(tuple(csv_line))
    return csv_array


def parse_json_or_string(input_str: str) -> object | str:
    try:
        ret = json.loads(input_str)
        return ret
    except ValueError:
        return input_str


def parse_bytes(input_str: str) -> bytes:
    return input_str.encode()


def parse_bool(input_str: str) -> bool:
    if input_str.lower() == "true":
        return True
    elif input_str.lower() == "false":
        return False


def parse_string_with_replace(input_str: str, replace_dict: dict) -> str:
    if input_str in replace_dict:
        return replace_dict[input_str.lower()]
    else:
        return input_str
