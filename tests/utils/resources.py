import os
from tests.utils.parsers import parse_csv


def get_resource_path(*args):
    root = os.environ['TEST_RESOURCES_ROOT']
    return os.path.join(root, *args)


def load_test_data(relative_resources_path, *args, **kwargs):
    root = os.environ['TEST_RESOURCES_ROOT']
    full_path = os.path.join(root, relative_resources_path)
    return lambda: parse_csv(full_path, args, **kwargs)
