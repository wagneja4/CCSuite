from lark import Lark

from ccsuite.server.repl.parser.parser_base import ParserBase
from ccsuite.server.repl.parser.transformer.default_transformer import REPLTransformerDefault


class ParserDefault(ParserBase):
    def __init__(self, path='resources/repl.ebnf'):
        with open(path) as fp:
            ebnf = fp.read()
        parser = Lark(ebnf, start='repl_line')
        transformer = REPLTransformerDefault()
        super().__init__(parser, transformer)

    def parse(self, string):
        ast = self.parser.parse(string)
        obj = self.transformer.transform(ast)
        return obj


if __name__ == '__main__':
    parser = ParserDefault()
    print(parser.parse('remote client exec PATH'))
