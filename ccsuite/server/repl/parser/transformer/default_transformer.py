from lark import Transformer


def create_repl_command_kind(kind, payload):
    return {'kind': kind, 'payload': payload}


def create_repl_command(command, args):
    return {'command': command, 'args': args}


class REPLTransformerDefault(Transformer):
    def remote_command(self, remote_command):
        client_identifier, remote_command_kind = remote_command
        return create_repl_command_kind('remote', dict(**self.remote_command_kind(remote_command_kind),
                                                       **{'target': client_identifier}))

    def valid_client_identifier(self, identifier):
        return identifier

    def valid_local_path(self, local_path):
        return local_path[0]

    def valid_exec(self, arg):
        return arg

    def valid_exec_arg(self, arg):
        return arg[0]

    def remote_command_kind(self, remote_command_kind):
        return remote_command_kind

    def remote_command_upload(self, valid_chanel_path):
        return create_repl_command('upload', valid_chanel_path)

    def remote_command_exec(self, valid_exec_arg):
        return create_repl_command('exec', valid_exec_arg)

    def local_command(self, local_command):
        # todo: likely bad gramar? hack: local_command[0]
        return create_repl_command_kind('local', local_command[0])

    def local_command_get(self, valid_chanel_path):
        return create_repl_command('get', valid_chanel_path)

    def local_command_cat(self, valid_chanel_path):
        return create_repl_command('cat', valid_chanel_path)

    def local_command_online(self, notning):
        return create_repl_command('online', notning)

    def local_command_result(self, job_id):
        return create_repl_command('result', job_id)

    def INT(self, value):
        return int(value)

    def WORD(self, value):
        return str(value)

    def valid_chanel_path(self, path):
        return path[0]

    def valid_client_identifier(self, path):
        return path[0]
