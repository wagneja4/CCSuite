import os
from ccsuite.ccchanel.ccchanel_base import CCChanelBase


def assert_is_path(string):
    if not isinstance(string, str):
        raise TypeError("path is not a string")

    if len(string) == 0:
        raise ValueError("path is empty string")


class CCChanelFile(CCChanelBase):
    def write(self, data: bytes, *args, **kwargs) -> None:
        if len(args) != 1:
            raise ValueError("Number of *args has to be one")

        f_path = args[0]
        assert_is_path(f_path)
        if os.path.isdir(f_path):
            raise IsADirectoryError(f_path)

        with open(f_path, "wb") as fp:
            fp.write(data)
        return

    def read(self, identifier: str, *args, **kwargs) -> bytes:
        assert_is_path(identifier)
        if os.path.isdir(identifier):
            raise IsADirectoryError(identifier)

        with open(identifier, "rb") as fp:
            return fp.read()

    def list(self, identifier: str, *args, **kwargs) -> [str]:
        assert_is_path(identifier)
        if not os.path.isdir(identifier):
            raise NotADirectoryError(identifier)
        return os.listdir(identifier)

    #todo: strip exists of directory check and create is writable instead, as exists could be used for checking before list as well
    def exists(self, identifier: str, *args, **kwargs) -> bool:
        assert_is_path(identifier)
        exists = os.path.exists(identifier)
        if os.path.isdir(identifier):
            raise IsADirectoryError(identifier)
        return exists


if __name__ == '__main__':
    file = '/tmp/ccchanel.txt'
    obj = b'sada'
    base = CCChanelFile()
    print(base.write(obj, file))
    print(base.read(file))
    print(base.list('/tmp'))
    print(base.exists(file))
